Responsibles :
Sam Meehan <samuel.meehan@cern.ch> - ATLAS
Shin Shan Yu <syu@cern.ch> - CMS
Tongyan Lin <tongylin@gmail.com> - Theory
Ulrich Haisch <Ulrich.Haisch@physics.ox.ac.uk> - Theory

In spring of 2017, ATLAS and CMS tried to cross check each other for their cross sections for the Zp2HDM model.  It was discovered that ATLAS had changed from the choice used in Run1, where all 2HDM higgs masses were held fixed to the same value as the pseudoscalar mass.  In the Run2 configuration, they are using the choice of m(h+)=m(h2)=300 GeV everywhere.  This has quite an effect on the branching ratio of A-->STUFF since as m(A) increases, additional channels open up.  This also has an effect on the kinematics of the signals and so a simple scaling of cross sections was not available.  Therefore, included here are the configurations and cross sections for both scenarios

CrossSections_20170517_ATLASCMS_Run1Parameters_HiggsMassesFixedToMA.txt
==> This is the "agreed" upon choice and consistent with what was used in Run1

CrossSections_20170517_ATLAS_Run1Parameters_HiggsMassesFixedTo300.txt
==> This is what ATLAS is using for the Spring 2017 result

Discussions with Uli Haisch determined that neither choice is more correct than the other.  

In the end, we hope to converge on using the "HiggsMassesFixedToMA" choice in the future.